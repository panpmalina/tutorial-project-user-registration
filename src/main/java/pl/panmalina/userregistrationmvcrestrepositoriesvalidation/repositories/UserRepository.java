package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    boolean existsByLogin(String login);
}
