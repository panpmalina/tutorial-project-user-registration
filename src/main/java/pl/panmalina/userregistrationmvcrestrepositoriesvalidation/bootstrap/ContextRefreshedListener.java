package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model.User;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.repositories.UserRepository;

@Component
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {

    UserRepository userRepository;

    public ContextRefreshedListener(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        User user1 = new User("aleks111", "passasd".toCharArray(), "Alex");
        User user2 = new User("brain222", "passaas".toCharArray(), "Brain");
        User user3 = User.builder()
                .login("jacek2")
                .password("pasdaas".toCharArray())
                .name("Jacek")
                .build();

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
    }
}
