package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.services;

import org.springframework.stereotype.Service;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model.Car;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CarService {

    List<Car> cars = new ArrayList<>();

    @PostConstruct
    public void initMockData() {
        Car car1 = Car.builder()
                .setId(1)
                .setName("BMW")
                .setYear(1925)
                .build();

        Car car2 = Car.builder()
                .setId(2)
                .setName("AUDI")
                .setYear(1930)
                .build();

        Car car3 = Car.builder()
                .setId(1)
                .setName("RENAULT")
                .setYear(1935)
                .build();

        cars = Arrays.asList(car1, car2, car3);
    }

    public Car getCarById(long id) {
        return cars.get((int) id - 1);
    }

}
