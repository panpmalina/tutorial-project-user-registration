package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.controllers.LoginAlreadyExistException;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model.User;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.repositories.UserRepository;

@Log4j2
@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isLoginExist(User user){
        return userRepository.existsByLogin(user.getLogin());
    }

    public String create(User user){
        if(isLoginExist(user)) {
            log.info(String.format("Login %s already exist", user.getLogin()));
            throw new LoginAlreadyExistException(user.getLogin());
        }
        userRepository.save(user);
        return String.format("Added new user %s", user.getLogin());
    }
}

