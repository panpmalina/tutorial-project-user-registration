package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model;

import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Getter @Setter @NoArgsConstructor
@AllArgsConstructor @ToString
@Builder
public class User extends BaseEntity {

    @NotBlank
    @Size(min = 5, max = 10)
    String login;

    @Size(min = 5, max = 15)
    char[] password;

    @NotBlank
    String name;
}

