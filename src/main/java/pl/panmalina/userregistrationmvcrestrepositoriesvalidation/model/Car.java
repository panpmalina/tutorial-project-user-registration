package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model;

public class Car {

    private long id;
    private String name;
    private int year;

    private Car(long id, String name, int year) {
        this.id = id;
        this.name = name;
        this.year = year;
    }

    public static CarBuilder builder() {
        return new CarBuilder();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                '}';
    }

    public static class CarBuilder {
        private long id;
        private String name;
        private int year;

        public Car build(){
            return new Car(id, name, year);
        }

        public CarBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public CarBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CarBuilder setYear(int year) {
            this.year = year;
            return this;
        }
    }

}
