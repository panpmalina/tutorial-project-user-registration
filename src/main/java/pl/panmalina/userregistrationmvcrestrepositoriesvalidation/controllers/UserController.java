package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.controllers;


import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model.User;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.services.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@Log4j2
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/add")
    public String createUser(@Valid @RequestBody User user) {
        return userService.create(user);
    }

    @RequestMapping("/show")
    public String show(){
        log.info("Show...");
        return "Show";
    }
}
