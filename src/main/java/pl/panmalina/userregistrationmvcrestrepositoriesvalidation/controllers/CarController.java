package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.model.Car;
import pl.panmalina.userregistrationmvcrestrepositoriesvalidation.services.CarService;

@RestController
@RequestMapping("/cars")
@Log4j2
public class CarController {

    private CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public Car get(@PathVariable long id) {
        log.info(String.format("Request: Get car with id: %d", id ));
        Car car = carService.getCarById(id);
        log.info(car);
        return car;
    }

}
