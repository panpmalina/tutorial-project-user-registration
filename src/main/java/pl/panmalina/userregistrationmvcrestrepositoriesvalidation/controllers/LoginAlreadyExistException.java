package pl.panmalina.userregistrationmvcrestrepositoriesvalidation.controllers;

public class LoginAlreadyExistException extends RuntimeException {

    public LoginAlreadyExistException(String login) {
        super(String.format("Login %s already exist", login));
    }
}
