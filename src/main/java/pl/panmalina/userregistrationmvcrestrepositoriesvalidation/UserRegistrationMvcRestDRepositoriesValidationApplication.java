package pl.panmalina.userregistrationmvcrestrepositoriesvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserRegistrationMvcRestDRepositoriesValidationApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserRegistrationMvcRestDRepositoriesValidationApplication.class, args);
    }

}
